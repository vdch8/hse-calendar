"""
This script runs the HseICalendar application using a flask server.
"""

from os import environ
from HseICalendar import createApp
from HseICalendar.icon import Icon
from HseICalendar.ruz.ruz import RUZ
from os import chdir, path

if __name__ == '__main__':
    chdir(path.dirname(__file__))

    try:
        HOST = environ.get('SERVER_HOST', 'localhost')
    except ValueError:
        HOST = 'localhost'
    try:
        PORT = int(environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555

    icon = Icon(HOST, PORT)
    icon.Run()

    person = RUZ()

    app = createApp(icon, person)
    app.run(HOST, PORT)
