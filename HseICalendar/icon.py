from pystray import Menu, MenuItem, Icon as PyIcon
from os import _exit
from PIL import Image
from webbrowser import open as BrowserOpen


class Icon:

    def __init__(self, serverHost, serverPort):
        self.baseURL = f'http://{serverHost}:{serverPort}'
        self.iconImage = Image.open('./HseICalendar/templates/icon.png')

    def Run(self):
        self.icon = PyIcon('HSE ICalendar', icon=self.iconImage,
                           title='HSE ICalendar',
                           menu=Menu(
                               MenuItem(
                                   'Open app',
                                   lambda icon, item: BrowserOpen(
                                       self.baseURL),
                                   default=True),
                               MenuItem(
                                   'Open settings',
                                   lambda icon, item: BrowserOpen(
                                       f'{self.baseURL}/settings')),
                               MenuItem(
                                   'Exit app',
                                   lambda icon, item: _exit(0))))
        self.icon.run_detached()

    def SendNotification(self, msg):
        self.icon.notify(msg)
