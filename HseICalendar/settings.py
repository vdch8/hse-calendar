"""
Routes and views for the settings page.
"""

from flask import current_app, render_template, Blueprint,\
    redirect, url_for, request, flash


settings = Blueprint('settings', __name__)


@settings.route('/settings')
def index():
    currentPageTitle = "Settings"

    return render_template('settings.html', currentPageTitle=currentPageTitle,
                           baseURL=request.host_url)


@settings.route('/settings/change', methods=['POST'])
def change():
    name = str(request.form.get('name'))
    update = str(request.form.get('update'))
    ruzid = str(request.form.get('ruzid'))
    personType = str(request.form.get('type'))

    if not len(name) or ruzid == "-1":
        return redirect(url_for('settings.index'))

    current_app.config['PERSON'].name = name
    current_app.config['PERSON'].ruzid = ruzid
    current_app.config['PERSON'].update = update
    current_app.config['PERSON'].type = personType
    current_app.config['PERSON'].WriteChanges()

    return redirect(url_for('main.index'))
