"""
Routes for the navbar.
"""

from flask import current_app, render_template,\
    Blueprint, redirect, url_for, request

actions = Blueprint('actions', __name__)


@actions.route('/actions/forceupdate')
def forceupdate():
    if current_app.config['PERSON'].ruzid == "-1":
        current_app.config['ICON'].SendNotification("Fill name in settings")
        return redirect(url_for('settings.index'))
    current_app.config['ICON'].SendNotification("Force update started")
    current_app.config['PERSON'].Download()
    return redirect(url_for('main.index'))


@actions.route('/actions/clearold')
def clearold():
    if current_app.config['PERSON'].ruzid == "-1":
        current_app.config['ICON'].SendNotification("Fill name in settings")
        return redirect(url_for('settings.index'))
    current_app.config['ICON'].SendNotification("Clear started")
    current_app.config['PERSON'].ClearOld()
    return redirect(url_for('main.index'))


@actions.route('/actions/toggle')
def toggle():
    if current_app.config['PERSON'].ruzid == "-1":
        current_app.config['ICON'].SendNotification("Fill name in settings")
        return redirect(url_for('settings.index'))
    if current_app.config['PERSON'].synchronization == "0":
        current_app.config['ICON'].SendNotification(
            "Synchronization enabled")
        current_app.config['PERSON'].synchronization = "1"
    else:
        current_app.config['ICON'].SendNotification("Synchronization disabled")
        current_app.config['PERSON'].synchronization = "0"

    current_app.config['PERSON'].WriteChanges()

    return redirect(url_for('main.index'))
