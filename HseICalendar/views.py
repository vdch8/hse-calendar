"""
Routes and views for the flask application.
"""

from flask import current_app, render_template, Blueprint, redirect, url_for
from datetime import datetime

main = Blueprint('main', __name__)


@main.route('/')
def index():
    currentPageTitle = "Index"
    lastUpdate = datetime.fromtimestamp(
        int(current_app.config['PERSON'].lastUpdate))\
        .strftime("%d/%m/%Y, %H:%M:%S")
    return render_template('index.html',
                           currentPageTitle=currentPageTitle,
                           syncDate=lastUpdate)
