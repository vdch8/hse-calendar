import requests
import os
from datetime import datetime
from dateutil.relativedelta import relativedelta
from threading import Thread
from time import sleep
import keyring


class RUZ:
    updateTimeDict = {
        "0": 30*60,
        "1": 60*60,
        "2": 2*60*60,
        "3": 4*60*60,
        "4": 8*60*60,
        "5": 24*60*60,
        "6": 7*24*60*60
    }

    def __init__(self):
        if keyring.get_password("HseICalendar", "synchronization") is None:
            keyring.set_password("HseICalendar", "synchronization", "0")

        if keyring.get_password("HseICalendar", "date") is None:
            startDate = (datetime.now() - relativedelta(months=2)
                         ).strftime("%Y.%m.%d")
            keyring.set_password("HseICalendar", "date", startDate)

        if keyring.get_password("HseICalendar", "ruzid") is None:
            keyring.set_password("HseICalendar", "ruzid", "-1")

        if keyring.get_password("HseICalendar", "update") is None:
            keyring.set_password("HseICalendar", "update", "0")

        if keyring.get_password("HseICalendar", "type") is None:
            keyring.set_password("HseICalendar", "type", "person")

        if keyring.get_password("HseICalendar", "lastupdate") is None:
            keyring.set_password("HseICalendar", "lastupdate", "0")

        self.ruzid = keyring.get_password("HseICalendar", "ruzid")
        self.synchronization = keyring.get_password(
            "HseICalendar", "synchronization")
        self.name = keyring.get_password("HseICalendar", "name")
        self.startDate = keyring.get_password("HseICalendar", "date")
        self.update = keyring.get_password("HseICalendar", "update")
        self.type = keyring.get_password("HseICalendar", "type")
        self.lastUpdate = keyring.get_password("HseICalendar", "lastupdate")

        thread = Thread(target=self.Runner)
        thread.start()

    def Download(self):
        endDate = (datetime.now() + relativedelta(months=2)
                   ).strftime("%Y.%m.%d")
        url = f'https://ruz.hse.ru/api/schedule/{self.type}/{self.ruzid}.ics?\
            start={self.startDate}&finish={endDate}&lng=1'
        r = requests.get(url)
        if not os.path.exists(os.path.abspath(
                f'./HseICalendar/static/schedules')):
            os.mkdir(os.path.abspath(f'./HseICalendar/static/schedules/'))
        with open(os.path.abspath(f'./HseICalendar/static/schedules/RUZ.ics'),
                  'wb+') as f:
            f.write(r.content)
        self.lastUpdate = str(int(datetime.now().timestamp()))
        self.WriteChanges()

    def ClearOld(self):
        self.startDate = datetime.now().strftime("%Y.%m.%d")
        self.WriteChanges()
        self.Download()

    def WriteChanges(self):
        keyring.set_password("HseICalendar", "ruzid", self.ruzid)
        keyring.set_password(
            "HseICalendar", "synchronization", self.synchronization)
        keyring.set_password("HseICalendar", "name", self.name)
        keyring.set_password("HseICalendar", "date", self.startDate)
        keyring.set_password("HseICalendar", "update", self.update)
        keyring.set_password("HseICalendar", "type", self.type)
        keyring.set_password("HseICalendar", "lastupdate", self.lastUpdate)

    def Runner(self):
        while True:
            if self.synchronization == "1":
                timestamp = int(datetime.now().timestamp())
                if timestamp - int(self.lastUpdate) >\
                        self.updateTimeDict[self.update]:
                    self.Download()
            sleep(60)
